<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



Route::get('admin_layout', function(){
	return View::make('layouts.admin');
});
Route::get('front_layout',function(){
	$subcategories=Subcategory::all();
	$categories = Category::all();
	$cart=Session::get('cart');
	$quantity=count($cart);
	$productWithCount = [];
	$arrayId=[];
	$arrayQuantity=[];
	$length = count($cart);
		if (count($cart) > 0)
		{
			for ($index = 0; $index < $length; $index++) {
				if (isset($cart[$index])) {
					$id = $cart[$index];
					unset($cart[$index]);
					$quantity1 = 1;
				for($j = $index + 1; $j < $length; $j++) {
					if (isset($cart[$j]) && $cart[$j] == $id) {
						$quantity1 ++;
						unset($cart[$j]);
					}
				}
				$productWithCount[] = [$id => $quantity1];
				$arrayId[]=$id;
				}
			}
		}
		$items= Product::whereIn("id", $arrayId)->get();
	return View::make('layouts.front',compact('categories','cart','quantity','items','productWithCount','quantity1'));
});
Route::get('admin_index', function(){
	return View::make('admin.index');
});
Route::get('front_index',function(){
	$categories = Category::all();
	$products = Product::all();
	$cart=Session::get('cart');
	$quantity=count($cart);
	$productWithCount = [];
	$arrayId=[];
	$arrayQuantity=[];
	$length = count($cart);
		if (count($cart) > 0)
		{
			for ($index = 0; $index < $length; $index++) {
				if (isset($cart[$index])) {
					$id = $cart[$index];
					unset($cart[$index]);
					$quantity1 = 1;
				for($j = $index + 1; $j < $length; $j++) {
					if (isset($cart[$j]) && $cart[$j] == $id) {
						$quantity1 ++;
						unset($cart[$j]);
					}
				}
				$productWithCount[] = [$id => $quantity1];
				$arrayId[]=$id;
				}
			}
		}
		$items= Product::whereIn("id", $arrayId)->get();
	return View::make('front.index', compact('categories','cart','quantity','products','items','productWithCount','quantity1'));
});

// Route::group(["before" => "auth"], function(){

Route::resource('orders', 'OrdersController');

Route::resource('users', 'UsersController');

Route::resource('products', 'ProductsController');

Route::resource('categories', 'CategoriesController');

// });
Route::get('category/{id}',function($id){
		// category -> subcategory
		$subcategories = Subcategory::where("category_id", "=", $id)->get();
		//Moi subcategory lay tat ca cac san pham
		$subId = [];
		foreach($subcategories as $subcategory) { 
			$subId[] = $subcategory->id;
		}
		$products = Product::whereIn("subcategory_id", $subId)->get();
		$categories = Category::all();
		$cart=Session::get('cart');
	 	$quantity=count($cart);
		$productWithCount= [];
			$arrayId=[];
			$arrayQuantity=[];
			$length = count($cart);
			$total = 0;
			$categories = Category::all();
				if (count($cart) > 0)
				{
					for ($index = 0; $index < $length; $index++) {
						if (isset($cart[$index])) {
							$id = $cart[$index];
							unset($cart[$index]);
							$quantity1 = 1;
						for($j = $index + 1; $j < $length; $j++) {
							if (isset($cart[$j]) && $cart[$j] == $id) {
								$quantity1 ++;
								unset($cart[$j]);
							}
						}
						$product = Product::find($id);
						$total += $product->price * $quantity1;
						$productWithCount[] = [$id => $quantity1];
						$arrayId[]=$id;
						}
					}
				}
				$items= Product::whereIn("id", $arrayId)->get();
	return View::make('front.dienthoai',compact('categories','cart','quantity','products','items','productWithCount','quantity1'));
});
	Route::get('category/brand/{id}',function($id){
		// category -> subcategory
		// $subcategories = Subcategory::where("category_id", "=", $id)->get();

		// //Moi subcategory lay tat ca cac san pham
		// $subId = [];
		// foreach($subcategories as $subcategory) { 
		// 	$subId[] = $subcategory->id;
		// }
		//$subcategories = Subcategory::where("category_id", "=", $cat_id)->get();
		$products = Product::where("subcategory_id", $id)->get();   
	 $categories = Category::all();
	 $cart=Session::get('cart');
	 $quantity=count($cart);
	$productWithCount= [];
	$arrayId=[];
	$arrayQuantity=[];
	$length = count($cart);
	$total = 0;
	$categories = Category::all();
		if (count($cart) > 0)
		{
			for ($index = 0; $index < $length; $index++) {
				if (isset($cart[$index])) {
					$id = $cart[$index];
					unset($cart[$index]);
					$quantity1 = 1;
				for($j = $index + 1; $j < $length; $j++) {
					if (isset($cart[$j]) && $cart[$j] == $id) {
						$quantity1 ++;
						unset($cart[$j]);
					}
				}
				$product = Product::find($id);
				$total += $product->price * $quantity1;
				$productWithCount[] = [$id => $quantity1];
				$arrayId[]=$id;
				}
			}
		}
		$items= Product::whereIn("id", $arrayId)->get();

	return View::make('front.dienthoai',compact('categories','cart','quantity','products','items','productWithCount','quantity1'));
});
	Route::get('admin', 'UsersController@getLogin');
	Route::post('admin', 'UsersController@postLogin');
	Route::get('admin/logout', function(){
		return View::make('admin.logins');
});
Route::get('cart', function(){
	$cart=Session::get('cart');
	// $cart=[1,2,3];
 	$quantity=count($cart);
	// return View::make('orders.session',['quantity'=>count($cart)]);
	return View::make('orders.session',compact('quantity'));
	// return Redirect::to('front_index',compact('quantity'));
});
Route::get('cart-detail', function(){
	$cart=Session::get('cart');
	return View::make('orders.session-detail',compact("cart"));
	// return Redirect::to('front_index', compact('cart'));
});
Route::get('add-item/{id}', function($id){
	if(Session::has('cart')) {
		$cart=Session::get('cart');
	} else {
		$cart=[];
	}
	$cart[]=$id;
	Session::put('cart',$cart);
	// return Redirect::to('cart');
	return Redirect::to('front_index');
});
Route::get('checkout', function(){
	// Save session into table "items" and "order" in DB
	
	// Remove session
	Session::forget('cart');
	
	//Redirect to front_index
	return Redirect::to('front_index');

});
Route::get('remove/{id}', function($id){
	$cart = Session::get('cart');
	// dd($cart);
	unset($cart[$id]);
	Session::set('cart', $cart);
	return Redirect::to('front_index');

});

Route::get('cart-list', function(){
	$cart=Session::get('cart');
	$productWithCount = [];
	$arrayId=[];
	$arrayQuantity=[];
		// [
		// 	[$id1, $quantity1],
		// 	[$id2, $quantity2],
		// 	[$id3, $quantity3],
		// 	[$id4, $quantity4],
		// var_dump($cart);
	// Chỉ loop khi cart còn item
	$length = count($cart);
		if (count($cart) > 0)
		{
		// dd($cart);
			for ($index = 0; $index < $length; $index++) {
				if (isset($cart[$index])) {
					$id = $cart[$index];
					unset($cart[$index]);
					$quantity1 = 1;
				for($j = $index + 1; $j < $length; $j++) {
					if (isset($cart[$j]) && $cart[$j] == $id) {
						$quantity1 ++;
						unset($cart[$j]);
					}
				}
				$productWithCount[] = [$id => $quantity1];
				$arrayId[]=$id;
				}
			}
		}
		$items= Product::whereIn("id", $arrayId)->get();
		// dd($items);

			// var_dump($productWithCount);	
			// var_dump($arrayId);
			// var_dump($arrayQuantity);
	//Lây item đầu tiên của cart

	// Loại bỏ item này đi

	// Loop phần còn lại của cart

	//Loại bỏ những phần tử giống item đầu tiên

	//Thực hiện lại đến khi cart ko còn phần tử nào
	return Redirect::to('front_index',compact('items','productWithCount','quantity1'));
	// return View::make('cart-list',compact('cart','items','productWithCount','arrayId','quantity'));
});
Route::get('cart-view', function(){
	$cart=Session::get('cart');
	$quantity=count($cart);
	$productWithCount= [];
	$arrayId=[];
	$arrayQuantity=[];
	$length = count($cart);
	$total = 0;
	$categories = Category::all();
		if (count($cart) > 0)
		{
			for ($index = 0; $index < $length; $index++) {
				if (isset($cart[$index])) {
					$id = $cart[$index];
					unset($cart[$index]);
					$quantity1 = 1;
				for($j = $index + 1; $j < $length; $j++) {
					if (isset($cart[$j]) && $cart[$j] == $id) {
						$quantity1 ++;
						unset($cart[$j]);
					}
				}
				$product = Product::find($id);
				$total += $product->price * $quantity1;
				$productWithCount[] = [$id => $quantity1];
				$arrayId[]=$id;
				}
			}
		}
		$items= Product::whereIn("id", $arrayId)->get();

	return View::make('orders.cartlist',compact('cart','items','productWithCount','arrayId','quantity1','quantity','categories', "total"));
});
Route::get('order', function(){
	return View::make('orders.create1');
});
