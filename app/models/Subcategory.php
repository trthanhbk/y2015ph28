<?php


class Subcategory extends Eloquent  {

	protected $fillable = ['name','category_id'];

	public function category()
    {
        return $this->belongsTo('Category');
    }
    

}
