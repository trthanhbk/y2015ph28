<?php

class Order extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'total_price' => 'required',
		'name' => 'required',
		'email' => 'required',
		'phone' => 'required'
	);
}
