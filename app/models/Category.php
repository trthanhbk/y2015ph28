<?php


class Category extends Eloquent  {

	protected $fillable = ['name'];

    public function subcategories()
    {
        return $this->hasMany('Subcategory');
    }



}
