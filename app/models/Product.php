<?php



class Product extends Eloquent  {

	protected $fillable = [ 'id', 'subcategory_id','name' , 'detail', 'price', 'color', 'image'];

	public static $rules = [];

	public function subcategory()
    {
        return $this->belongsTo('Subcategory');
    }

}
