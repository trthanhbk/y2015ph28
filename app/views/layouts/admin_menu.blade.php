                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="{{url('admin_index')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{action('CategoriesController@index')}}"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý sản phẩm<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{action('CategoriesController@index')}}">Category</a>
                                </li>
                                <li>
                                    <a href="{{action('ProductsController@index')}}">Product</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i>Quản lý bài viết</a>
                        </li>
                        
                        <li>
                            <a href="{{action('OrdersController@index')}}"><i class="fa fa-dashboard fa-fw"></i>Quản lý đơn hàng</a>
                        </li>

                        <li>
                            <a href="{{action('UsersController@index')}}"><i class="fa fa-dashboard fa-fw"></i>Quản lý người dùng</a>
                        </li>
                       
                    </ul>
                </div>