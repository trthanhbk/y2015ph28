@include('front.header')
  <div class="wrap">
	<div class="header">
		<div class="headertop_desc">
			<div class="call">
				 <p>Tư vấn miễn phí<span class="number">1-22-3456789</span></span></p>
			</div>
			<div class="account_desc">
				<ul>
					<li><a href="#">ĐĂNG NHẬP</a></li>
					<li><a href="#">ĐĂNG KÝ</a></li>
					<li><a href="#">GIỎ HÀNG</a></li>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
		<div class="header_top">
			<div class="logo">
				<a href="index.html"><img src="{{url('assets/front/images/logo.png')}}" alt="" /></a>
			</div>
			  <div class="cart">
			  	   	<p><span>Giỏ hàng:</span>
			  	   	<div id="dd" class="wrapper-dropdown-2"> {{$quantity}} sản phẩm
			  	   	<ul class="dropdown">
						@if (!$productWithCount)
							<li>{{'Hiện giỏ hàng chưa có sản phẩm nào...'}}</li>
						@else
						@for ($i = 0; $i < count($productWithCount); $i++)
			  	   		<li>                			
                			<div style="float:left;width:100%;">
	                			<a href=""> 
	                    			<img width="40" height="40" src="{{url('asset/images',$items[$i]->image)}}">                    
	               				</a>
	               				<a href="#">{{$items[$i]->name}}</a></div>
                			<div style="float:right;">
                			@foreach ($productWithCount[$i] as $id => $quantity1)
								{{$quantity1}}
							@endforeach 
							× {{$items[$i]->price}}
							</div>
            			</li>
						@endfor
						<div style="float:left;width:100%; margin:20px;">Total:  </div>
    					<div style="float:left;margin:0px 0 20px	 20px;">
        					<a href="{{url('cart-view')}}">View Cart →</a>
        					<a href="{{url('order')}}">Proceed to Checkout</a>
    					</div>
						@endif
					</ul>
					</div>
					</p>
			  </div>
			  
	 <div class="clear"></div>
  </div>
	@include('front.menu')
	<div class="header_slide">
			<div class="header_bottom_left">
				<div class="categories">
				  <ul>
				  		<h3>DANH MỤC SẢN PHẨM</h3>
				      	@foreach($categories as $category)
							<li><a href="{{url('category',$category->id)}}">{{$category->name}}</a>
							<ul>
							@foreach ($category->subcategories as $subcategory)
								<li><a href="{{url('category/brand',$subcategory->id)}}">{{$subcategory->name}}</a></li>
								@endforeach
							</ul></li>
						@endforeach
				  </ul>
				</div>
	  	     </div>
					 <div class="header_bottom_right">
					 	 <div class="slider">
							 <div id="slider">
			                    <div id="mover">
			                    	<div id="slide-1" class="slide">
									 <div class="slider-img">
									     <a href="preview.html"><img src="{{url('assets/front/images/slide-1-image.png')}}" alt="learn more" /></a>
									  </div>
						             	<div class="slider-text">
		                                 <h1>Clearance<br><span>SALE</span></h1>
		                                 <h2>UPTo 20% OFF</h2>
									   <div class="features_list">
									   	<h4>Get to Know More About Our Memorable Services Lorem Ipsum is simply dummy text</h4>
							            </div>
							             <a href="preview.html" class="button">Shop Now</a>
					                   </div>
									  <div class="clear"></div>
				                  </div>
						             	<div class="slide">
						             		<div class="slider-text">
		                                 <h1>Clearance<br><span>SALE</span></h1>
		                                 <h2>UPTo 40% OFF</h2>
									   <div class="features_list">
									   	<h4>Get to Know More About Our Memorable Services</h4>
							            </div>
							             <a href="preview.html" class="button">Shop Now</a>
					                   </div>
						             	 <div class="slider-img">
									     <a href="preview.html"><img src="{{url('assets/front/images/slide-3-image.jpg')}}" alt="learn more" /></a>
									  </div>
									  <div class="clear"></div>
				                  </div>
				                  <div class="slide">
					                  <div class="slider-img">
									     <a href="preview.html"><img src="{{url('assets/front/images/slide-2-image.jpg')}}" alt="learn more" /></a>
									  </div>
									  <div class="slider-text">
		                                 <h1>Clearance<br><span>SALE</span></h1>
		                                 <h2>UPTo 10% OFF</h2>
									   <div class="features_list">
									   	<h4>Get to Know More About Our Memorable Services Lorem Ipsum is simply dummy text</h4>
							            </div>
							             <a href="preview.html" class="button">Shop Now</a>
					                   </div>
									  <div class="clear"></div>
				                  </div>
			                 </div>
		                </div>
					 <div class="clear"></div>
		         </div>
		      </div>
		   <div class="clear"></div>
		</div>
   </div>
 <div class="main">
    @yield('content')
 </div>
</div>
   @include('front.footer')