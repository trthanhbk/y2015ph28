<!DOCTYPE html>
<html>
<head>
    <title>Login Admin</title>
</head>
<body>
	<table border="0" width="100%">
    	<tr>
        	<td colspan="3" style="background-color:black; color: white; font-size:250%; text-align:center; height:100px;">Login Admin</td>
        </tr>
        <tr>
        	<td style="width:100px"></td>
            <td>
                <body>
                @if(Session::has("message"))
                  {{ Session::get("message")}}
                @endif
                <form method="POST">
                <table width="670" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                         <td>
                           <table width="100%" border="0" cellspacing="2" cellpadding="2">
                           <tbody>
                               <tr>
                               <td align="right">Tên đăng nhập :</td>
                                  <td><input type="text" name="email" id="email" class="textfield" style="width:162px;" placeholder="Email"></td>
                                  @include('admin.line_error',["field" => "email"])
                                </tr>
                                <tr>
                                 <td align="right">Mật khẩu :</td>
                                 <td><input type="password" name="password" id="password" class="textfield" style="width:162px;" placeholder="Password"></td>
                                   @include('admin.line_error',["field" => "password"])
                                </tr>
                                <tr>
                                  <td align="right"><input type="submit" value="Login"></td>
                                </tr>
                            </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                </form>
                </body>
    </table>
</body>
</html>
          