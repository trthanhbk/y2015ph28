@if(count($errors->get($field)))  
   <div style="color: red">
     @foreach($errors->get($field) as $error)
        <div>{{ $error }}</div>
     @endforeach
  </div>
@endif