@extends('layouts.front')

@section('content')
<div class="content">
    	<div class="content_top"> 
    		<div class="heading">
    		<h3>SẢN PHẨM BÁN CHẠY NHẤT</h3>
    		</div>
    		<div class="see">
    			<p><a href="#">Xem thêm</a></p>
    		</div>
    		<div class="clear"></div>
    	</div>
	      <div class="section group">
	        @foreach($products as $product)
				<div class="grid_1_of_4 images_1_of_4">
					 <a href="preview.html"><img src="{{url('assets/front/images',$product->image)}}" alt="" /></a>
					 <h2>{{$product->name}}</h2>
					<div class="price-details">
				       <div class="price-number">
							<p><span class="rupees">{{$product->price}}</span></p>
					    </div>
					       		<div class="add-cart">								
									<h4><a href="{{url('add-item',$product->id)}}">Mua ngay</a></h4>						
							     </div>
							 <div class="clear"></div>
					</div>
			</div>
			@endforeach	
			</div>
			<div class="content_bottom">
    		<div class="heading">
    		<h3>SẢN PHẨM MỚI</h3>
    		</div>
    		<div class="see">
    			<p><a href="#">Xem thêm</a></p>
    		</div>
    		<div class="clear"></div>
    	</div>
			<div class="section group">
			@foreach($products as $product)
				<div class="grid_1_of_4 images_1_of_4">
					 <a href="preview.html"><img src="{{url('assets/front/images',$product->image)}}" alt="" /></a>
					 <h2>{{$product->name}}</h2>
					<div class="price-details">
				       <div class="price-number">
							<p><span class="rupees">{{$product->price}}</span></p>
					    </div>
					       		<div class="add-cart">								
									<h4><a href="{{url('add-item',$product->id)}}">Mua ngay</a></h4>						
							     </div>
							 <div class="clear"></div>
					</div>
					 
				</div>
				@endforeach
		</div>
			
    </div>
@stop