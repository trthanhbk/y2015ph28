<!DOCTYPE HTML>
<head>
<title>Free Home Shoppe Website Template | Home :: w3layouts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="{{url('assets/front/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('assets/front/css/bootstrap-theme.min.css')}}">
<link href="{{url('assets/front/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{url('assets/front/css/slider.css')}}" rel="stylesheet" type="text/css" media="all"/>

</head>
<body>