<div class="footer">
   	  <div class="wrap">	
	     <div class="section group">
				<div class="col_1_of_4 span_1_of_4">
						<h4>THÔNG TIN CỬA HÀNG</h4>
						<ul>
						<li><a href="about.html">VỀ CHÚNG TÔI</a></li>						
						<li><a href="contact.html">LIÊN HỆ</a></li>
						</ul>
					</div>
				<div class="col_1_of_4 span_1_of_4">
					<h4>DỊCH VỤ VÀ HỖ TRỢ</h4>
						<ul>
						<li><a href="about.html">HƯỚNG DẪN MUA HÀNG</a></li>
						<li><a href="contact.html">HƯỚNG DẪN THANH TOÁN</a></li>
						<li><a href="#">CÂU HỎI THƯỜNG GẶP</a></li>
						<li><a href="contact.html">HỆ THỐNG CỦA HÀNG</a></li>
						</ul>
				</div>
				<div class="col_1_of_4 span_1_of_4">
					<h4>CHÍNH SÁCH</h4>
						<ul>
							<li><a href="contact.html">CHÍNH SÁCH GIAO HÀNG</a></li>
							<li><a href="index.html">CHÍNH SÁCH BẢO HÀNH</a></li>
							<li><a href="#">CHÍNH SÁCH ĐỔI SẢN PHẨM</a></li>
							<li><a href="#">HƯỚNG DẪN MUA HÀNG</a></li>
							<li><a href="contact.html">CÂU HỎI THƯỜNG GẶP</a></li>
						</ul>
				</div>
				<div class="col_1_of_4 span_1_of_4">
					<h4>LIÊN HỆ</h4>
						<ul>
							<li><span>+91-123-456789</span></li>
							<li><span>+00-123-000000</span></li>
						</ul>
						<div class="social-icons">
							<h4>KẾT NỐI VỚI CHÚNG TÔI</h4>
					   		  <ul>
							      <li><a href="#" target="_blank"><img src="{{url('assets/front/images/facebook.png')}}" alt="" /></a></li>
							      <li><a href="#" target="_blank"><img src="{{url('assets/front/images/twitter.png')}}" alt="" /></a></li>
							      <li><a href="#" target="_blank"><img src="{{url('assets/front/images/skype.png')}}" alt="" /> </a></li>
							      <li><a href="#" target="_blank"> <img src="{{url('assets/front/images/dribbble.png')}}" alt="" /></a></li>
							      <li><a href="#" target="_blank"> <img src="{{url('assets/front/images/linkedin.png')}}" alt="" /></a></li>
							      <div class="clear"></div>
						     </ul>
   	 					</div>
				</div>
			</div>			
        </div>
        <div class="copy_right">
				<p>Company Name © All rights Reseverd | Design by  <a href="http://w3layouts.com">W3Layouts</a> </p>
		   </div>
    </div>

    <a href="#" id="toTop"><span id="toTopHover"> </span></a>
    <script type="text/javascript" src="{{url('assets/front/js/jquery-1.7.2.min.js')}}"></script> 
<script type="text/javascript" src="{{url('assets/front/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{url('assets/front/js/easing.js')}}"></script>
<script type="text/javascript" src="{{url('assets/front/js/startstop-slider.js')}}"></script>
    <script type="text/javascript">
		$(document).ready(function() {			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<script type="text/javascript">
			function DropDown(el) {
				this.dd = el;
				this.initEvents();
			}
			DropDown.prototype = {
				initEvents : function() {
					var obj = this;

					obj.dd.on('click', function(event){
						$(this).toggleClass('active');
						event.stopPropagation();
					});
				}
			}

			$(function() {

				var dd = new DropDown( $('#dd') );

				$(document).click(function() {
					// all dropdowns
					$('.wrapper-dropdown-2').removeClass('active');
				});

			});

		</script>
</body>
</html>

