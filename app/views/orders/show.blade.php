@extends('layouts.admin')

@section('content')

<h1>Show Order</h1>

<p>{{ link_to_route('orders.index', 'Return to All orders', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Total_price</th>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $order->total_price }}}</td>
					<td>{{{ $order->name }}}</td>
					<td>{{{ $order->email }}}</td>
					<td>{{{ $order->phone }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('orders.destroy', $order->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('orders.edit', 'Edit', array($order->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
