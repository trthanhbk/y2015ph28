
@extends('layouts.front')
@section('content')
<table class="table table-hover">
	<thead>
        <tr>
            <th class="product-remove">&nbsp;</th>
            <th class="product-name">Sản phẩm</th>
           	<th class="product-quantity">Số lượng</th>
            <th class="product-subtotal">Giá</th>
        </tr>
	</thead>
		@for ($i = 0; $i < count($productWithCount); $i++)
	<tr>
		<!-- Remove from cart link -->
		<td>
			<a href="#">X</a>
		</td>
		<!-- Product Name -->
		<td>
			<!-- The thumbnail -->
			<div>
				<a href="#"><img width="65" height="65" src="{{url('asset/images',$items[$i]->image)}}"></a>
			</div>
			<div>
			<div>
				<a href="#">{{$items[$i]->name}}</a>      
			</div>
			<!-- Product price -->
			<div>
				<span>{{$items[$i]->price}}</span>
			</div>
			</div>
		</td>
		<!-- Quantity inputs -->
		@foreach($productWithCount[$i] as $id => $quantity1)
		<td>
			<div>
				{{$quantity1}}
			</div>
		</td>
		<!-- Product subtotal -->
		<td>
			<span>
		{{$quantity1}}x{{$items[$i]->price}}
			</span>
		</td>
		@endforeach 
	</tr>
	@endfor 
	<tr>
		<td></td><td></td><td></td>
		<td>
			{{$total}}
		</td>
	</tr>                  
</table>
@stop