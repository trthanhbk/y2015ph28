@extends('layouts.admin')

@section('content')

<h1>All Orders</h1>

<p>{{ link_to_route('orders.create', 'Add New Order', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($orders->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Total_price</th>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($orders as $order)
				<tr>
					<td>{{{ $order->total_price }}}</td>
					<td>{{{ $order->name }}}</td>
					<td>{{{ $order->email }}}</td>
					<td>{{{ $order->phone }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('orders.destroy', $order->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('orders.edit', 'Edit', array($order->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no orders
@endif

@stop
