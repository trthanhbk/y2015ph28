@extends('layouts.admin')

@section('content')

<h1>Show Product</h1>

<p>{{ link_to_route('products.index', 'Return to All products', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Subcategory</th>
				<th>Name</th>
				<th>Detail</th>
				<th>Price</th>
				<th>Color</th>
				<th>Image</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $product->subcategory->name }}}</td>
					<td>{{{ $product->name }}}</td>
					<td>{{{ $product->detail }}}</td>
					<td>{{{ $product->price }}}</td>
					<td>{{{ $product->color }}}</td>
					<td>{{{ $product->image }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('products.destroy', $product->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('products.edit', 'Edit', array($product->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
