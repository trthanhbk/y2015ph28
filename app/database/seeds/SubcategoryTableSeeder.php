<?php

class SubcategoryTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('subcategories')->truncate();

		Subcategory::create(['name' => 'APPLE', "category_id" => '1']);
        Subcategory::create(['name' => 'SAMSUNG', "category_id" => '1']);
        Subcategory::create(['name' => 'ACER', "category_id" => '2']);
        Subcategory::create(['name' => 'APPLE', "category_id" => '2']);
        Subcategory::create(['name' => 'LG', "category_id" => '1']);
        Subcategory::create(['name' => 'OPPO', "category_id" => '1']);
        Subcategory::create(['name' => 'NOKIA', "category_id" => '1']);
        Subcategory::create(['name' => 'SONY', "category_id" => '1']);
        Subcategory::create(['name' => 'SAMSUNG', "category_id" => '2']);
	}

}
