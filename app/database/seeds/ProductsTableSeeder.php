<?php
class ProductsTableSeeder extends Seeder {

    		public function run()
    		{
       			DB::table('products')->truncate();

        		Product::create(['subcategory_id' =>'1','name' => 'iPhone 6 16GB', 'detail' => 'FullBox 16GB', 'price' =>'16.999.000', 'color'=>'Gold','image' =>'iphone.png']);
        		Product::create(['subcategory_id' =>'8','name' => 'SONY XPERIA M4', 'detail' => 'FullBox 32GB', 'price' =>'6.490.000đ', 'color'=>'Gold','image' =>'2.png']);
        		Product::create(['subcategory_id' =>'2','name' => 'SAMSUNG GALAXY E5', 'detail' => 'FullBox 32GB', 'price' =>'4.890.000đ', 'color'=>'Gold','image' =>'3.jpeg']);
        		Product::create(['subcategory_id' =>'6','name' => 'OPPO NEO 5', 'detail' => 'FullBox 32GB', 'price' =>'3.690.000đ', 'color'=>'Gold','image' =>'4.png']);
        		Product::create(['subcategory_id' =>'7','name' => 'LUMINA 640XL', 'detail' => 'FullBox 32GB', 'price' =>'4.589.000đ', 'color'=>'Gold','image' =>'5.jpg']);
        		Product::create(['subcategory_id' =>'5','name' => 'LG G4 LEATHER', 'detail' => 'FullBox 32GB', 'price' =>'15.990.000đ', 'color'=>'Gold','image' =>'6.jpeg']);
        		Product::create(['subcategory_id' =>'2','name' => 'SAMSUNG GALAXY S6', 'detail' => 'FullBox 32GB', 'price' =>'16.590.000đ', 'color'=>'Gold','image' =>'7.png']);
                Product::create(['subcategory_id' =>'2','name' => 'SAMSUNG GALAXY A7', 'detail' => 'FullBox 32GB', 'price' =>'9.990.000đ', 'color'=>'Gold','image' =>'8.jpeg']);
                Product::create(['subcategory_id' =>'4','name' => 'iPad Mini 3 Retina Cellular 16GB', 'detail' => 'FullBox 16GB', 'price' =>'13.590.000đ', 'color'=>'Gold','image' =>'ipad-mini-3-cellular.jpg']);
                Product::create(['subcategory_id' =>'1','name' => 'iPhone 4S 8GB', 'detail' => 'FullBox 8GB', 'price' =>'4.999.000đ', 'color'=>'Gold','image' =>'ip_4s_8.png']);
                Product::create(['subcategory_id' =>'1','name' => 'iPhone 5S 16GB', 'detail' => 'FullBox 16GB', 'price' =>'12.990.000đ', 'color'=>'Gold','image' =>'ip_5s_16.png']);
                Product::create(['subcategory_id' =>'1','name' => 'iPhone 5S 16GB', 'detail' => 'FullBox 16GB', 'price' =>'19.599.000đ', 'color'=>'Gold','image' =>'ip_6p_16.png']);
                Product::create(['subcategory_id' =>'2','name' => 'Samsung Galaxy S6 Edge 64GB', 'detail' => 'FullBox 64GB', 'price' =>'20.990.000đ', 'color'=>'Gold','image' =>'s6_edge.png']);
                Product::create(['subcategory_id' =>'5','name' => 'LG Magna', 'detail' => 'FullBox 64GB', 'price' =>'3.990.000đ', 'color'=>'Gold','image' =>'lg_magna.png']);
                Product::create(['subcategory_id' =>'6','name' => 'Oppo Find 7A', 'detail' => 'FullBox 64GB', 'price' =>'8.990.000đ', 'color'=>'Gold','image' =>'oppo_find_7a.jpeg']);
                Product::create(['subcategory_id' =>'6','name' => 'Oppo R5', 'detail' => 'FullBox 64GB', 'price' =>'9.999.000đ', 'color'=>'Gold','image' =>'oppo_r5.png']);
                Product::create(['subcategory_id' =>'6','name' => 'OPPO N1 Mini', 'detail' => 'FullBox 64GB', 'price' =>'8.490.000đ', 'color'=>'Gold','image' =>'n1-mini.jpg']);
                Product::create(['subcategory_id' =>'7','name' => 'Nokia Lumia 930', 'detail' => 'FullBox 64GB', 'price' =>'10.990.000đ', 'color'=>'Gold','image' =>'lumia_930.jpg']);
                Product::create(['subcategory_id' =>'7','name' => 'Nokia Lumia 730', 'detail' => 'FullBox 64GB', 'price' =>'3.990.000đ', 'color'=>'Gold','image' =>'lumia_730.jpeg']);
                Product::create(['subcategory_id' =>'7','name' => 'Nokia Lumia 535', 'detail' => 'FullBox 64GB', 'price' =>'2.990.000đ', 'color'=>'Gold','image' =>'lumia_535.jpg']);
                Product::create(['subcategory_id' =>'8','name' => 'Sony Xperia Z3', 'detail' => 'FullBox 64GB', 'price' =>'14.990.000đ', 'color'=>'Gold','image' =>'Sony-Xperia-Z3-Plus-Dual.png']);
                Product::create(['subcategory_id' =>'8','name' => 'Sony Xperia Z2', 'detail' => 'FullBox 64GB', 'price' =>'10.990.000đ', 'color'=>'Gold','image' =>'Z2-4G.jpeg']);
                Product::create(['subcategory_id' =>'8','name' => 'SSony Xperia M2', 'detail' => 'FullBox 64GB', 'price' =>'4.990.000đ', 'color'=>'Gold','image' =>'sony-m2.png']);
                Product::create(['subcategory_id' =>'4','name' => 'iPad Air 2 Wifi 4G 64GB', 'detail' => 'FullBox 64GB', 'price' =>'17.599.000đ', 'color'=>'Gold','image' =>'iPadAir2.png']);
                Product::create(['subcategory_id' =>'4','name' => 'iPad mini 16GB Wifi', 'detail' => 'FullBox 64GB', 'price' =>'5.990.000đ', 'color'=>'Gold','image' =>'ipad_mini.JPG']);
                Product::create(['subcategory_id' =>'9','name' => 'Samsung Galaxy Tab A 9.7 inch', 'detail' => 'FullBox 64GB', 'price' =>'8.990.000đ', 'color'=>'Gold','image' =>'samsung-galaxy-tab-a-97.jpg']);
                Product::create(['subcategory_id' =>'9','name' => 'Samsung Galaxy Tab 3V T116', 'detail' => 'FullBox 64GB', 'price' =>'3.490.000đ', 'color'=>'Gold','image' =>'Samsung-Galaxy-Tab-3.jpg']);
                Product::create(['subcategory_id' =>'9','name' => 'Samsung Galaxy Tab A 8.0 inch', 'detail' => 'FullBox 64GB', 'price' =>'6.990.000đ', 'color'=>'Gold','image' =>'TabA9.png']);
        		Product::create(['subcategory_id' =>'9','name' => 'Samsung T805 - Tab S 10.5 inch', 'detail' => 'FullBox 64GB', 'price' =>'11.490.000đ', 'color'=>'Gold','image' =>'ss_t805.jpeg']);
    		}

	}